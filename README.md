# Snakemake workflow: ngs-test-data

This workflow creates small test datasets for NGS data analyses. The generated data is available in the folders `ref` and `reads`, such that the repository can be directly used as a git submodule for continuous integration tests.

## Authors

* Sebastian Schmeier adjusted from Johannes Köster (https://github.com/snakemake-workflows/ngs-test-data)


## Install

To add this module as a submodule to a differnt workflow do this in your workflow directory base directory.

```bash
git submodule add git@gitlab.com:schmeierlab/workflows/ngs-test-data.git
git submodule update --init --recursive
```
